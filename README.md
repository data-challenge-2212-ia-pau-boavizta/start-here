<div align="center">
  <a href="https://boavizta.org/">
    <img src="assets/boavizta_logo.png" width="160" height="160">
  </a>
</div>


# Boavizta's challenge

**Welcome to the 2022 Data Challenge hosted by IA Pau!** 🎉

The [SoftAWARE](https://gitlab.com/softawere-hackathon/softawere/) tool chain is open source and developed by the [SDIA](https://sdialliance.org/), [Boavizta](https://boavizta.org/) and [Hubblo](https://hubblo.org/). 

➡️ The Data Challenge is over! [All projects are available in read-only mode](https://gitlab.com/groups/data-challenge-2212-ia-pau-boavizta/-/archived).


## Some useful links

🖼 [Introduction slides](https://docs.google.com/presentation/d/1HBP3XNYiUZd9xlCmJFOwFK9X9Twt2qt5LIkAW8nv5SA/edit?usp=sharing)

🎯 [Leaderboard](http://leaderboard.metal.re:8506/dashboard/#/nc/view/bb1da1a2-2dc4-4275-8326-1d6a5126bacd)

📊 [Training Dataset](https://www.dropbox.com/s/856qixcbdt0zeam/train.csv?dl=0)

*Optional dataset for language modeling tasks: [wikitext](https://huggingface.co/datasets/wikitext)*


## Requirements

1. Git ([Windows](https://git-scm.com/download/win) | [MacOS](https://git-scm.com/download/mac) | [Linux](https://git-scm.com/download/linux))
2. GitLab Account ([create account](https://gitlab.com/users/sign_up))
3. Python >= 3.9 ([Windows](https://www.python.org/downloads/windows/) | [MacOS](https://www.python.org/downloads/macos/))
4. DVC - manage datasets and models ([Windows](https://dvc.org/doc/install/windows) | [MacOS](https://dvc.org/doc/install/macos) | [Linux](https://dvc.org/doc/install/linux))

For each team, please send all GitLab user handle in one mail to [samuel@rince.me](mailto:samuel@rince.me). This step is mandatory to be added to the GitLab Group. You can find your GitLab handle when you are logged in:

<img src="assets/gitlab_handle.png" width="300">

## Installation

**⚠️ The installation process must be done by only 1 member of the group. Except when adding DVC credentials**

Video tutorial 🎬:

[![](https://img.youtube.com/vi/5-CtN7VM9D8/0.jpg)](https://www.youtube.com/watch?v=5-CtN7VM9D8 "Video tutorial")

### Create a new project in data-challenge-2212-ia-pau-boavizta group

Create a new project, clicking on the "new project" button

<div align="center">
  <img src="assets/new_project.png" width="500">
</div>

Click on "Create blank project" and uncheck "Initialize repository with a README"

<div align="center">
  <img src="assets/create_project.png" width="500">
</div>

### Import files from `base-project` to your new repo

All commands containing `<VARIABLES>` require that you adapt the command to your use case. For instance in this step you must replace `<MY_PROJECT_NAME>` by your project name (e.g. `my-team`). `<MY_PROJECT_NAME>` must not contain any whitespace or other specials characters.

Clone the repository:

```shell
git clone https://gitlab.com/data-challenge-2212-ia-pau-boavizta/base-project.git <MY_PROJECT_NAME>
cd <MY_PROJECT_NAME>
```

Remove the default origin remote:

```shell
git remote rm origin
```

Create the new remote origin:

```shell
git remote add origin <MY_NEW_REPO_URL>
```

Push the files to the new repo:

```shell
git push --set-upstream origin main
```

### Update DVC configuration

Each group will be provided with secret tokens to store you models on a distant server. These tokens are strictly private and must not be used in any other way intended during the Data Challenge.

Change remote URL:

```
dvc remote modify dcremote url s3://data-challenge-model-bucket/<MY_PROJECT_NAME>
```

Add authentication tokens:

**⚠️ All team members must do this step.**

```shell
dvc remote modify --local dcremote access_key_id <ACCESS_KEY_ID>
dvc remote modify --local dcremote secret_access_key <SECRET_ACCESS_KEY>
```

Push DVC configuration update:

```shell
git add .dvc/config
git commit -m "chore: update dvc url"
git push
```


## Usage

### Submit a new model to the leaderboard

First you need to add you model in the `models/` directory. All big files (like weights) must be handle using DVC.

Add model to DVC:

```shell
dvc add models/my-experiment/model.bin
```

Then push the model to DVC:

```shell
dvc push
```

When it's done you can commit your model card:

```shell
git add models/my-experiment/model.bin.dvc
git commit -m "feat: add my experiment model"
```

Adapt the `evaluation.py` script to your needs. It should load your model, execute on the whole dataframe containing the testset and then save the **output in CSV file called `prediction.csv` with 2 columns (id, label)**.

How `prediction.csv` should look like:

| id | label |
|----|-------|
| 0  | 0     |
| 1  | 0     |
| 2  | 1     |
| 3  | 0     |

Don't forget to add your dependencies (torch, tensorflow, ...) to the `requirements.txt` file.

Commit and push your updates:

```shell
git add ...
git commit -m "..."
git push
```

Start submission job:

<div align="center">
  <img src="assets/click_ci_cd.png" width="500">
</div>

<div align="center">
  <img src="assets/start_ci_job.png" width="500">
</div>

### Download models from DVC

Let's say that one of your teammate has added a new model and uploaded it using DVC. If you want it on you local machine you just to pull it (just like git) using the following command:

```shell
dvc pull
```

### Speed-up you submissions

You can change the base image used in the `.gitlab-ci.yml` (line 2) depending on the python libraries you are going to use.

Default image is python3.9 with some basic libraries like pandas or dvc.

```yaml
default:
  image: registry.gitlab.com/data-challenge-2212-ia-pau-boavizta/base-runner-container/python3.9-slim-ci:latest
```

Replace the `image` link with one of the followings if it matches your usecase:

* For pytorch: `registry.gitlab.com/data-challenge-2212-ia-pau-boavizta/base-runner-container/python3.9-slim-torch11.3-cu116-ci:latest`
* For tensorflow: `registry.gitlab.com/data-challenge-2212-ia-pau-boavizta/base-runner-container/python3.9-slim-tensorflow2.11-cu116-ci:latest`
* To add other libraries and base models like nltk or spacy for instance, please ask by mail to [samuel@rince.me](mailto:samuel@rince.me)
